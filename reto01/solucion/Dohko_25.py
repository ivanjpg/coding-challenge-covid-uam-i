# programa que determina si un año es bisiesto
# Aqui se usa el hecho de que (p & ¬q) | r

año=int(input('Introduzca el año que desea saber si es bisiesto'))
if (año%4==0 and año%100!=0) or año%400 == 0:
    print('Sí es bisiesto')
else:
    print('No es bisiesto')

    # mismo programa usando "&" y "|"

año=int(input('Introduzca el año que desea saber si es bisiesto'))
if (año%4==0 & año%100!=0) | año%400 == 0:
    print('Sí es bisiesto')
else:
    print('No es bisiesto')
    # Duda para el que lea esto: por qué el resultado difiere si uso "and" "or"(correcto)
#que al usar "&" y "|"  (incorrecto)

#Aquí la otra forma usando el hecho de que p & (¬q | r) usando
#las propiedades de asociatividad en lógica

año=int(input('Introduzca el año que desea saber si es bisiesto'))
if año%4==0 and (año%100!=0 or año%400 == 0):
    print('Sí es bisiesto')
else:
    print('No es bisiesto')

    # A diferencia del caso anterior aquí sí puedo hacer uso de cualquier simbología sea
# "and" "or" o "&" y "|" como se ve a continuación

año=int(input('Introduzca el año que desea saber si es bisiesto'))
if año%4==0 & (año%100!=0 | año%400 == 0):
    print('Sí es bisiesto')
else:
    print('No es bisiesto')
