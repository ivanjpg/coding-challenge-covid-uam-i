again=True # Bandera para preguntar si desea continuar validando otros años

while again: # Condición para continuar validando años
    flag=True # Bandera para validar si el input es un número entero
   
    ############ Valida el año, no puede ser negativo ni contener caracteres #################
    while flag:
        x=input('Ingresa un año para analizarlo: ')
        try: # Si el año no puede convertise en un numero entero, volvera a pedir como input el año
            x=int(x)
            if type(x) == int and x>=0:
                flag = False
                break
        except:
            continue
    ################# Fin de la validación del año ###################

    if (x%4==0 and x%100!=0) or x%400 == 0 : # Año bisiesto
        print('El año {} es bisiesto :D'.format(x))
    else:
        print('El año {} no es bisiesto :('.format(x))
    
    continuar=input('Quieres probar con otro año?, s/n: ') # Input para continuar validando años
    continuar=continuar.upper() # convierte 's' a 'S'
    # Cualquier input diferente a 's' o 'S' sera tomado como "no continuar"
    if continuar == 'S': 
        again=True
    else:
        again=False

    
