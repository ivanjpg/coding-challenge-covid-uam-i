#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
========================================================
                 ¿Es año bisiesto?
========================================================
       
               12 / Abril / 2020   3:12
       
Autor: Alberto Medina   
"""

y = int(input("\n¿Qué año desea comprobar?\n\n"))

p  = ( y % 4   ) == 0 #  p = Es divisible entre 4
cq = ( y % 100 ) != 0 # cq = No es divisible entre 100
r  = ( y % 400 ) == 0 #  r = Es divisible entre 400

if ( p and cq ) or r:    
   print('\nEl año',y,'es bisiesto\n')
else:
   print('\nEl año',y,'no es bisiesto\n')
