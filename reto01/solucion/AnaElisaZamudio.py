#Realizar un programa que indique si el año ingresado es un año bisiesto

#Un año es bisiesto si (p\land\lnot q) \lor r, P= divisible entre 4, q= no es divisible entre 100
#r= es divisible entre 400

Year=int(input("Ingrese el año..."))

p = (Year) % 4
q = (Year) % 100
r = Year % 400

if (p == 0 and q > 0 or r==0):
    print("Año bisiesto")
else: print("No es año bisisesto")
