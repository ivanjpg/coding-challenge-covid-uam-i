# Reto 01
## ¿Es año bisiesto?

Este primer reto consiste en realizar un programa que indique al usuario si algún año es bisiesto.

Considerando las premisas:

- **p** = Es divisible entre 4.
- **q** = No es divisible entre 100.
- **r** = Es divisible entre 400.

Entonces un año es bisiesto si:

$$(p \land \lnot q) \lor r$$

**Nota**: Esto no aplica para el calendario juliano, pero en este caso no lo consideramos.

**Extras**:

- Se podría preguntar al usuario qué año desea comprobar.
- Podría leerse el año a comprobarse desde la linea de comandos.

## Comentarios generales de las soluciones

- No sé si queriendo o de manera fortuita, varios utilizaron *ñ* para escribir el nombre su variable **año**, no está mal, de hecho la mayoría de los lenguajes modernos admiten a UTF-8 como codificación de caracteres y más allá, como caracteres válidos para los nombres de los identificadores. Los viejitos como yo estábamos acostumbrados a no poner acentos, *n*, así como caracteres *raros* fuera de las letras del alfabeto inglés. Como mera curiosidad, en Swift, un lenguaje desarrollado por Apple para programar aplicaciones para MacOSX y dispositivos iOS (iPad, iPhone, ...) admite caracteres Unicode, es decir, podrían poner puros caracteres chinos y funciona.

- Los comentarios en Python comienzan con # si estos son en un sola linea. Si se requieren comentarios multilinea pueden utilizarse tres apóstrofes seguidos, para terminar el comentario será de la misma manera:

```python
'''
Este es un comentario
en varias líneas.
'''

# Este comentario únicamente puede estar en esta linea.
```

- La comprobación de la entrada del usuario resulta fundamental para el funcionamiento correcto del programa, y si bien, no era un requisito, varios participantes consideraron distintos casos. En particular, la solución *oficial* considera sólo el caso en que la entrada no es un número entero. Por simplicidad en varias soluciones se omitirán ciertas comprobaciones con el fin de hacer más didácticas las mismas.

- La lógica del programa en este caso fue establecida de manera clara y concisa desde un inicio para facilitar su codificación. No siempre será así y se dejará a discreción de cada participante la implementación de la misma.

## Preguntas de los participantes

- ¿Por qué si utilizo los operadores lógicos *and* y *or* obtengo un resultado, pero si utilizo **&** y **|** obtengo otro resultado diferente?

Los operadores **&** y **|** son operadores de bits. Para el caso de este problema al estar trabajando con comparaciones que devuelven falso o verdadero dan la impresión de funcionar igual que los operadores lógicos *and* y *or*. El detalle importante radica en la precedencia de los operadores, para nuestro caso particular primero se consideran los paréntesis, sigue el operador módulo (**%**), luego los operadores de bits (**&,|**), en seguida las comparaciones (**==, !=, <, >, <=, >=**) y al final los operadores lógicos (**not, and, or**). Entonces tenemos que estas expresiones se evalúan de diferente manera (se pueden ejecutar directamente en el intérprete interactivo de Python y ver los resultados):

```python
# Definimos el año con el que trabajaremos
año = 2004

año % 4 == 0 and año % 100 != 0
```

El código anterior nos devolverá *True* dado que *2004* es divisible entre *4* pero no entre *100*. Es decir, las dos sentencias a los lados de **and** son verdaderas y por ello el resultado es verdadero. Agregar paréntesis del siguiente modo no afecta dado que es el mismo orden de evaluación:

```python
año = 2004

(año % 4 == 0) and (año % 100 != 0)
```

Ahora trabajemos con los operadores de bits:

```python
# Definimos el año con el que trabajaremos
año = 2004

año % 4 == 0 & año % 100 != 0
```

Las instrucciones anteriores regresan *False* debido a que, por precedencia, se ejecutan las operaciones que contienen **%**, luego la operación con **&** y al final las comparaciones **==, !=**, entonces colocando paréntesis para dejar en claro:

```python
# Definimos el año con el que trabajaremos
año = 2004

(año % 4) == ( 0 & (año % 100) ) != 0
```

Lo cual da como resultado *False*, si dejamos los paréntesis pero cambiamos **&** por **and**:

```python
# Definimos el año con el que trabajaremos
año = 2004

(año % 4) == ( 0 and (año % 100) ) != 0
```

Que de igual manera regresa *False*.

En resumen: el problema es que **&, |** tienen mayor precedencia que *and, or* y por ello se altera el orden de evaluación, mejor dicho, no es el que esperamos.

- ¿Por qué funciona usando la asociatividad de la lógica propocicional?

La asociatividad en las operaciones lógicas únicamente aplica cuando los operadores involucrados son del mismo tipo, es decir, todos son **or** o todos son **y**, en este caso no puede aplicarse dado que tenemos una mezcla de ambos. Lo que podría hacerse es utilizar la distributividad, es decir:

$$
(p \land q) \lor r = (p \lor r) \land (q \lor r) 
$$

La tabla 2.4 del libro de Matemáticas Discretas de Favio Miranda y Elisa Viso contiene esta información. <https://bit.ly/matesdiscretas-elisa>

Entonces en realidad el usar la *asociatividad* para resolver el problema constituye un error en el tratamiendo de las condiciones lógicas del problema.

## Solución

***
```python
# Definimos los valores por defecto
# Año a comprobar
year = 2020

# Preguntamos al usuario el año a comprobar
# Las entradas desde el teclado siempre son de tipo cadena
year_str = input('¿Qué año desea comprobar si es bisiesto? ')

# Intentamos convertir a entero
try:
	year = int(year_str)

# Si la conversión no es exitosa enviamos un mensaje de
# error y tomamos el valor por defecto.
except:
	# La f antes de la cadena nos permite interpolar expresiones
	# válidas del lenguaje, como variables, constantes, llamadas
	# a funciones, y más. Esto siempre que se encuentren encerradas
	# entre {}
	print(f'Entrada no válida. Usando el valor por default {year}')

# El operador % es llamado módulo y devuelve el residuo de
# la división. Por lo que 2002%4 devolverá un 2, por ejemplo.
if ( (year % 4) == 0 and (year % 100) != 0 ) or (year % 400) == 0:
	print(f'{year} SÍ es bisiesto.')
else:
	print(f'{year} NO es bisiesto.')
```

***