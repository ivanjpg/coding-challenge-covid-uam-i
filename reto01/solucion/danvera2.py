#Juan Daniel Vera G.

# Programa que verifica si un año es bisiesto o no

# El programa consta de tres funciones principales
# La primera de ellas "valida_año" verifica que se haya ingresado un año valido (no letras o años negativos)
# La segunda de ellas "bisiesto" verifica si el año es bisiesto o no
# La tercera "again" verifica si el usuario desea continuar introduciendo años para evaluarlos
# Finalmente, el programa está dentro de un ciclo while que valida si es necesario volver a validar otro año o no

def valida_año(x):
    try:
        x=int(x) # si el año no es correctamente ingresado, el programa irá al except y regresara False
        if x>=0: #verifica si el año es mayor a cero
            return False 
        else:
            return True
    except:
        return True

#calculo de año bisiesto
def bisiesto(x):
    x=int(x)
    if (x%4==0 and x%100!=0) or x%400 == 0 : # Año bisiesto
        return True
    else:
        return False
#Pregunta si deseas continuar validando años 's' o 'S' permiten continuar
#Cualquier otro valor es tomado como 'no'

def again(answer):
    answer=answer.upper()
    if answer =='S' or answer == 'SI' or  answer == 'SÍ' :
        return True
    else:
        return False

#Comienza el programa
answer=True # Bandera de inicio

while answer:

    x=input('Ingresa el año: ')

    while valida_año(x): # Verifica que el año sea correctamente ingresado
        x=input('ingresa el año correctamente: ')

    result=bisiesto(x) # Verifica si el año es bisiesto

    # evalua el resultado
    if result == True:
        print('El año {} es bisiesto :D'.format(x))
    else:
        print('El año {} no es bisiesto :('.format(x))
    
    #Input para para continuar o finalizar el programa
    answer=again(input('Quieres probar con otro año?, s/n: '))

print('¡Adios! :D')
    






            