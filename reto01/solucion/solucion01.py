# Definimos los valores por defecto
# Año a comprobar
year = 2020

# Preguntamos al usuario el año a comprobar
# Las entradas desde el teclado siempre son de tipo cadena
year_str = input('¿Qué año desea comprobar si es bisiesto? ')

# Intentamos convertir a entero
try:
	year = int(year_str)

# Si la conversión no es exitosa enviamos un mensaje de
# error y tomamos el valor por defecto.
except:
	# La f antes de la cadena nos permite interpolar expresiones
	# válidas del lenguaje, como variables, constantes, llamadas
	# a funciones, y más. Esto siempre que se encuentren encerradas
	# entre {}
	print(f'Entrada no válida. Usando el valor por default {year}')

# El operador % es llamado módulo y devuelve el residuo de
# la división. Por lo que 2002%4 devolverá un 2, por ejemplo.
if ( (year % 4) == 0 and (year % 100) != 0 ) or (year % 400) == 0:
	print(f'{year} SÍ es bisiesto.')
else:
	print(f'{year} NO es bisiesto.')