#Este primer reto consiste en realizar un programa que indique al usuario si algún año es bisiesto.

#Considerando las premisas:

#- **p** = Es divisible entre 4.
#- **q** = No es divisible entre 100.
#- **r** = Es divisible entre 400.

#Entonces un año es bisiesto si:

#si p y q se cumplen 

#**Nota**: Esto no aplica para el calendario juliano, pero en este caso no lo consideramos.

#**Extras**:

#- Se podría preguntar al usuario qué año desea comprobar.
#- Podría leerse el año a comprobarse desde la linea de comandos.


print("¿Es año bisiesto?")
entrada="S"

while entrada=="S":
    ano=int(input("¿Que año quiere comprobar?: "))
    if ano%4==0:
         if ano%100==0:
             print("El año "+str(ano)+ " no es bisiesto")
             print("")
         else:
             print("El año "+str(ano)+ " es bisiesto")
             print("")
    elif ano%400==0:
         print("El año "+str(ano)+ " es bisiesto")
         print("")
    else:
          print("El año "+str(ano)+ " no es bisiesto")
          print("")
    entrada=str(input("¿Quiere comprobar otro año? [S/N] ")).upper()
    if entrada=="N":
        break

##aaron :)