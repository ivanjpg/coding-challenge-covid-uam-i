# Reto 01
## ¿Es año bisiesto?

Este primer reto consiste en realizar un programa que indique al usuario si algún año es bisiesto.

Considerando las premisas:

- **p** = Es divisible entre 4.
- **q** = No es divisible entre 100.
- **r** = Es divisible entre 400.

Entonces un año es bisiesto si:

$$(p \land \lnot q) \lor r$$

**Nota**: Esto no aplica para el calendario juliano, pero en este caso no lo consideramos.

**Extras**:

- Se podría preguntar al usuario qué año desea comprobar.
- Podría leerse el año a comprobarse desde la linea de comandos.