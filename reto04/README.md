# Reto 04
## Buscando sucesiones y sus sumas en $\pi$

Los dígitos decimales de $\pi$ son infinitos. A veces se dice que ahí adentro están metidas todas las obras escritas y por escribirse de la literatura, cualquier número telefónico, las fechas de cumpleaños, ...

Bueno, ha llegado nuestro momento, ¿por qué no buscamos alguna cosa dentro de los decimales conocidos de $\pi$?

Aprovechando la coyuntura actual (*'íralo*), por aquello que se anda diciendo de que la vida como la conocíamos terminó el trece de marzo, busquemos **diez** (10) dígitos consecutivos cuya suma sea **trece** (13) y determinemos su posición dentro de los decimales de $\pi$.

Para lo anterior he tomado un archivo con millón y medio (*maso*) de dígitos de $\pi$ elaborado por Scott Hemphill y que puede encontrarse en el proyecto Gutenberg https://bit.ly/cccuami-pi-125. **¡Pero!** yo lo modifiqué para que fuera más fácil su uso, quité comentarios, espacios, saltos de linea y la parte entera de $\pi$, es decir, *3.*. El archivo que les presento para el reto consiste en una sola linea larguísima con más de un millón de dígitos decimales de $\pi$ sin espacios, sin saltos de linea, lo encuentran [acá](https://bit.ly/cccuami-pi).

## Extras

- Se podría preguntar al usuario de qué tamaño es la sucesión que busca y cuál debe ser la suma de los dígitos de la misma.
- También podrían leerse los parámetros desde la linea de comandos.

## Pistas

- Cuando leen información desde un archivo (de texto) siempre el resultado es una cadena, siempre se trata de un **string**, si bien es útil para operaciones como hacer *slices*, si se quieren sumar hay que hacer la conversión a números, ya sea la cadena completa o una parte.
- La sucesión que buscamos se encuentra a partir del decimal **13721**.
- Los números buscados son: **2100222013**.