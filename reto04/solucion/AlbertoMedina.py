# -*- coding: utf-8 -*-
print("""
========================================================
    reto04: Buscando sucesiones y sus sumas en $\pi$
========================================================
       
               16 / Abril / 2020   21:18
       
Autor: Alberto Medina   

"Beethoven era un buen compositor porque utilizaba ideas nuevas en combinación 
con ideas antiguas. Nadie, ni siquiera Beethoven podría inventar la música 
desde cero. Es igual con la informática." - Richard Stallman""")

k    = int(input("\n¿De qué tamaño es la sucesión que busca?\n"))
sum0 = int(input("\n¿Cuál debe ser la suma de los dígitos de la misma?\n")); print('\n')

# El archivo "pi.txt" debe de estar en el mismo directorio que el programa     
lista = []
file = open('pi.txt') 
for x in file:
    lista.append(x.strip().split())
file.close()
a = lista[0][0]

def sum_dig(w):
    sum = 0
    for i in range(len(w)):
        sum = sum + int(w[i])
    return sum

j=0
for i in range(len(a)-k+1):
    b = a[i:k+i]
    if sum_dig(b) == sum0:
        j += 1
        print('Decimal: ',i+1,' Sucesión: ',b,' Suma: ',sum_dig(b))
if j==0:
   print('Error: Se requieren más dígitos de pi.')