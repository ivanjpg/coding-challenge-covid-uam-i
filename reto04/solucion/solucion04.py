# Declaramos una constante para el
# nombre del archivo donde están algunos
# decimales de Pi
PI_FILE = 'pi.txt'
# La suma que queremos encontrar
PI_SUM = 13
# ¿Cuántos dígitos deben formar la suma?
PI_DIGITS = 10

# Abrimos el archivo, por default
# se abre en modo lectura
f = open(PI_FILE)

# Leemos el archivo
# Se lee todo como una cadena
# de texto, pero las cadenas en
# Python podemos recorrerlas como si
# de un arreglo se tratara
decimales = f.read()

# Cerramos el archivo
f.close()

# Tamaño de la cadena leída, en
# este caso el número de decimales
# de Pi en el archivo
tamaño = len(decimales)

# Creamos una variable booleana
# para saber si hemos encontrado
# la sucesión de dígitos
encontrado = False

# Recorremos dígito por dígito
# tomando en cada paso ese dígito
# y los siguientes 9
for i in range(tamaño):
	print(f'\rAnalizando posición {i+1}/{tamaño} [{(i+1)/tamaño*100:.1f}%]...', end='')

	num_str = decimales[i:i+PI_DIGITS]

	# Recorremos cada uno de los 
	# 10 dígitos, los transformamos
	# en enteros y los sumamos en s
	s = 0
	for j in num_str:
		s += int(j)

	if s == PI_SUM:
		# Si encontramos dicha suma colocamos una marca y
		# lo informamos
		encontrado = True
		print(f'\n{PI_DIGITS} dígitos que suman {PI_SUM} se encuentran en el {i+1} decimal y la secuencia es: {num_str}.')
		# Y salimos del for
		# Debido al break sólo se encuentra la primer
		# secuencia de números que cumple con las condiciones
		# GabyDu señaló que el enunciado original del problema
		# no especificaba que sólo había que encontrar la primer
		# sucesión de números.
		# Si se comenta el break se encuentran todas las sucesiones.
		break

# Si no se encontró la secuencia, lo informamos
if not encontrado:
	print(f'\nNo se pudieron encontrar {PI_DIGITS} dígitos consecutivos en {tamaño} decimales de π cuya suma sea {PI_SUM} (ಥ﹏ಥ)')

