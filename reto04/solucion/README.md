# Reto 04
## Buscando sucesiones y sus sumas en $\pi$

Los dígitos decimales de $\pi$ son infinitos. A veces se dice que ahí adentro están metidas todas las obras escritas y por escribirse de la literatura, cualquier número telefónico, las fechas de cumpleaños, ...

Bueno, ha llegado nuestro momento, ¿por qué no buscamos alguna cosa dentro de los decimales conocidos de $\pi$?

Aprovechando la coyuntura actual (*'íralo*), por aquello que se anda diciendo de que la vida como la conocíamos terminó el trece de marzo, busquemos **diez** (10) dígitos consecutivos cuya suma sea **trece** (13) y determinemos su posición dentro de los decimales de $\pi$.

Para lo anterior he tomado un archivo con millón y medio (*maso*) de dígitos de $\pi$ elaborado por Scott Hemphill y que puede encontrarse en el proyecto Gutenberg https://bit.ly/cccuami-pi-125. **¡Pero!** yo lo modifiqué para que fuera más fácil su uso, quité comentarios, espacios, saltos de linea y la parte entera de $\pi$, es decir, *3.*. El archivo que les presento para el reto consiste en una sola linea larguísima con más de un millón de dígitos decimales de $\pi$ sin espacios, sin saltos de linea, lo encuentran [acá](https://bit.ly/cccuami-pi).

## Extras

- Se podría preguntar al usuario de qué tamaño es la sucesión que busca y cuál debe ser la suma de los dígitos de la misma.
- También podrían leerse los parámetros desde la linea de comandos.

## Pistas

- Cuando leen información desde un archivo (de texto) siempre el resultado es una cadena, siempre se trata de un **string**, si bien es útil para operaciones como hacer *slices*, si se quieren sumar hay que hacer la conversión a números, ya sea la cadena completa o una parte.
- La sucesión que buscamos se encuentra a partir del decimal **13721**.
- Los números buscados son: **2100222013**.

## Solución

***
```python
# Declaramos una constante para el
# nombre del archivo donde están algunos
# decimales de Pi
PI_FILE = 'pi.txt'
# La suma que queremos encontrar
PI_SUM = 13
# ¿Cuántos dígitos deben formar la suma?
PI_DIGITS = 10

# Abrimos el archivo, por default
# se abre en modo lectura
f = open(PI_FILE)

# Leemos el archivo
# Se lee todo como una cadena
# de texto, pero las cadenas en
# Python podemos recorrerlas como si
# de un arreglo se tratara
decimales = f.read()

# Cerramos el archivo
f.close()

# Tamaño de la cadena leída, en
# este caso el número de decimales
# de Pi en el archivo
tamaño = len(decimales)

# Creamos una variable booleana
# para saber si hemos encontrado
# la sucesión de dígitos
encontrado = False

# Recorremos dígito por dígito
# tomando en cada paso ese dígito
# y los siguientes 9
for i in range(tamaño):
	print(f'\rAnalizando posición {i+1}/{tamaño} \
		[{(i+1)/tamaño*100:.1f}%]...', end='')

	num_str = decimales[i:i+PI_DIGITS]

	# Recorremos cada uno de los 
	# 10 dígitos, los transformamos
	# en enteros y los sumamos en s
	s = 0
	for j in num_str:
		s += int(j)

	if s == PI_SUM:
		# Si encontramos dicha suma colocamos una marca y
		# lo informamos
		encontrado = True
		print(f'\n{PI_DIGITS} dígitos que suman {PI_SUM} se \
			encuentran en el {i+1} decimal y la secuencia es: \
			{num_str}.')
		# Y salimos del for
		# Debido al break sólo se encuentra la primer
		# secuencia de números que cumple con las condiciones
		# GabyDu señaló que el enunciado original del problema
		# no especificaba que sólo había que encontrar la primer
		# sucesión de números.
		# Si se comenta el break se encuentran todas las sucesiones.
		break

# Si no se encontró la secuencia, lo informamos
if not encontrado:
	print(f'\nNo se pudieron encontrar {PI_DIGITS} dígitos \
		consecutivos en {tamaño} decimales de π cuya suma sea \
		{PI_SUM} (ಥ﹏ಥ)')


```
***