#!/usr/bin/env python3
# -*- coding: utf-8 -*-
print("""
========================================================
                   reto05: Fibonacci
========================================================
       
               20 / Abril / 2020   21:18
       
Autor: Alberto Medina   

"Software is like sex: it's better when it's free." - Linus Torvalds""")

n    = int(input("\n¿Cuántos términos de la sucesión desea calcular ?\n"))
fibo = [0,1]
if 2 < n: 
   [fibo.append(fibo[i]+fibo[i+1]) for i in range(n-2)]
print('\n')
[print('Fibonacci[',i+1,'] =',fibo[i]) for i in range(n)]

k  = int(input("\n¿Qué término de la sucesión desea conocer?\n"))
if k <= n:
   print('\nFibonacci[',k,'] =',fibo[k-1]) 
else:
   print("\nError: El término de la sucesión debe ser menor o igual a",n)