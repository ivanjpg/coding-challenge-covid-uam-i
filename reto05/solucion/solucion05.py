# Construímos una función que nos devuelva el
# n-ésimo término de la sucesión.
# En este caso guardamos todos los términos
def fibonacci(n):
	# Definimos una lista (array) donde almacenamos
	# los elementos de la sucesión de Fibonacci
	# Los primeros dos términos están dados, 
	# son cero y uno.
	a = [0,1]

	# Si requerimos el término 0 o el término
	# 1, ya los tenemos, sólos los regresamos.
	if(n in [0,1]):
		return a[n]

	# Si se requiere algún otro término debemos calcularlo
	# range empieza en 2, ya tenemos 0 y 1.
	# Va hasta n+1 dado que la función range no incluye
	# a su segundo argumento, por ejemplo
	# range(0,4) -> 0, 1, 2, 3
	for i in range(2, n+1):
		# El índice es i, el siguiente término de
		# la sucesión se calcula sumando los dos
		# elementos anteriores.
		# append agrega un elemento al final
		# de la lista.
		a.append( a[i-1] + a[i-2] )

	# Devolvemos el término de la sucesión deseado
	return a[n]

# Podemos construír otra función donde
# únicamente se almacenen los dos términos
# anteriores al que se quiere calcular.
def fibonacci2(n):
	# Definimos dos variables
	# que guardarán los valores.
	# También podría utilizarse un arreglo
	a0 = 0
	a1 = 1

	# Si se quiere el término 0
	# de la sucesión lo regresamos.
	if n == 0:
		return a0

	# Lo mismo para el término 1
	if n == 1:
		return a1

	for i in range(2, n+1):
		# Necesitamos una variable temporal.
		# Guardamos en ella el valor del término
		# siguiente de la sucesión
		tmp = a0 + a1

		# Reemplazamos el término "-2" de la
		# sucesión
		a0 = a1

		# El término actual queda en a1 
		a1 = tmp

	return a1

# Definimos el valor por defecto
# Término deseado
n = 100

# Preguntamos al usuario qué término desea
# Las entradas desde el teclado siempre son de tipo cadena
n_str = input('¿Qué término de la sucesión de Fibonacci desea? ')

# Intentamos convertir a entero
try:
	n = int(n_str)
except:
	# La f antes de la cadena nos permite interpolar expresiones
	# válidas del lenguaje, como variables, constantes, llamadas
	# a funciones, y más. Esto siempre que se encuentren encerradas
	# entre {}
	print(f'Entrada no válida. Usando el valor por default {n}')

# Comprobamos que ambas funciones regresan el mismo valor. 
print(f'El {n} término de la sucesión de Fibonacci es: {fibonacci(n)}')
print(f'El {n} término de la sucesión de Fibonacci2 es: {fibonacci2(n)}')
print(f'Puedes compararlo con el que calcula WolframAlpha, visita https://www.wolframalpha.com/input/?i=fibonacci+{n}')
