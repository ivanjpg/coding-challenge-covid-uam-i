# Reto 05
## Fibonacci

Leonardo da Pisa utilizó una sucesión para el problema de la cría de conejos, en particular, para su crecimiento poblacional; esto en el año 1202. Cabe señalar que existen estudios [^1] donde se indica que los hindúes ya conocían esta sucesión.

[^1]: https://doi.org/10.1016/0315-0860(85)90021-7

Los primeros dos términos de la sucesión están dados, son cero y uno, los siguientes se calculan sumando los dos anteriores.

Lo que queremos es calcular el término n-ésimo de la sucesión:

$$
0, 1, 1, 2, 3, 5, 8, \ldots
$$

## Extras

- Se podría preguntar al usuario qué término de la sucesión quiere conocer.
- También podrían leerse los parámetros desde la linea de comandos.

## Pistas

- Los ciclos son sus amigos, para todo sirven.
- Se puede almacenar toda la sucesión en alguna estructura de datos o se pueden guardar sólamente los dos términos anteriores al que se está calculando.