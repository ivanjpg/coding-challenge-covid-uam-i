# CodingChallengeCOVIDUAM-I [cccuami]

## Pequeños retos de programación para aprender y pasar el rato durante la cuarentena debida a la COVID-19. [UAM-I]

El lenguaje seleccionado por una oficialísima votación en el H. grupo de Licenciatura y Posgrado de la UAM-I ha sido Python (no, no es phyton). A continuación listo unos pequeños e inocentes lineamientos para el buen desarrollo y sanísima convivencia entre los participantes:

- El lenguaje elegido es Python (por votación).
- La versión del lenguaje es 3, en cualquiera de sus variantes. Porfa, ya superen a la versión 2. Python 3 salió hace 11 años...
- Los retos que irán aumentando de dificultad; el tiempo límite para entregarlos se definirá en cada uno de los ejercicios. Tomaremos en cuenta la asincronía, no todos estamos conectados a la misma hora ni vemos los posts al mismo tiempo.
- Para facilitar la búsqueda utilizaremos el hashtag #CCCUAMI
- Los códigos serán enviados a: cccuami+retoXX@gmail.com donde XX es el número de reto, por ejemplo, el reto 3 se enviará al correo cccuami+reto03@gmail.com, el reto 13 a cccuami+reto13@gmail.com. Por favor indiquen como quieren que aparezca el crédito: un nickname, nombre real, email, ...
- Los que deseen participar quedan en el entendido de que sus programas serán públicos, serán organizados por retos y serán puestos a disposición de la comunidad en un repositorio de GitLab. Recuerden la frase de Richard Stallman: *Para aprender a programar hay que leer mucho código y escribir mucho código*.
- Quienes deseen colaborar enviando sus soluciones en lenguajes distintos, también son bienvenidos; la piedra de Rosetta permitió entender los jeroglíficos egipcios. ;)
- Al final de cada reto se publicará la **solución oficial** que no necesariamente es la mejor, pero es un buen punto de comparación para considerar enfoques diferentes. Irá completamente comentada destacando los puntos importantes.

Ojalá que todos lo disfruten y aprendan.

**Happy hacking!**
