# Definimos los valores por defecto
# Tamaño de la base
n = 5
# Caracter de dibujo
c = 'o'

# Preguntamos al usuario de qué tamaño quiere la base
# Las entradas desde el teclado siempre son de tipo cadena
n_str = input('¿De qué tamaño desea la base del triángulo? ')

# Intentamos convertir a entero
try:
	n = int(n_str)
except:
	# La f antes de la cadena nos permite interpolar expresiones
	# válidas del lenguaje, como variables, constantes, llamadas
	# a funciones, y más. Esto siempre que se encuentren encerradas
	# entre {}
	print(f'Entrada no válida. Usando el valor por default {n}')

# Preguntamos por el caracter de dibujo
c = input('¿Cuál es el caracter para dibujar? ')

# Dibujamos el triángulo
# Primero recorremos las filas
for i in range(0, n):
	# En Python3 existe el producto de cadenas por
	# un escalar (número entero en este caso),
	# el resultado es la cadena repetida el número
	# de veces indicado por el escalar.
	# Aquí se requiere sumar 1 dado que el ciclo
	# comienza desde cero.
	print( c * (i+1) )

# Otro enfoque es utilizar ciclos anidados.
# Recorremos las filas
for i in range(0, n):
	# Recorremos las columnas
	for j in range(0,i+1):
		# La función print siempre agrega un salto
		# de linea al final, en este caso no lo
		# queremos así, entonces pasamos un parámetro
		# llamado *end* donde indicamos que al final
		# de su salida agregue una cadena vacía
		print(c, end='')

	# Luego, al terminar cada fila debemos saltar a la
	# siguiente. Para ello llamamos a print sin argumentos,
	# lo cual imprime una cadena vacía con un salto de
	# linea al final.
	print()

