# Reto 02
## Dibujando un triángulo con 5 caracteres

Este reto consiste en dibujar en pantalla un tríangulo rectángulo cuya base mida *5* caracteres. Como ejemplo consideremos que el caracter que utilizaremos para dibujar será **o**, entonces nuestro programa deberá tener como salida:

    o
    oo
    ooo
    oooo
    ooooo

**Extras**

- Se podría preguntar al usuario de qué tamaño se quiere la base del triángulo y con qué caracter desea que dibujemos el triángulo.
- También podrían leerse los parámetros anteriores desde la linea de comandos.

**Pistas**:

- En la mayoría de los lenguajes de programación, los ciclos (*loops*) son fundamentales. También existen los ciclos anidados.
- En Python3 existe el producto de cadenas por un escalar (número entero en este caso), el resultado es la cadena repetida el número de veces indicado por el escalar.

## Comentarios generales de las soluciones

- Varios los resolvieron de manera muy rápida, pocos minutos después de haber sido propuesto el reto y todos funcionan: ¡Qué gusto!

- Varios imprimen una linea vacía de más, utiliza el operador de repetición para cadenas dentro de un **for** con un **range**, este último con un solo argumento comenzará a contar en cero, lo que al multiplicar por una cadena dará como resultado una cadena vacía, y esta a su vez dentro de un **print** mandará a pantalla la cadena vacía más un salto de linea al final de ella; generando así una linea vacía extra en la pantalla.

- Además algunos tenían saltos entre las filas, esto se resuelve usando un `print` con una cadena vacía o utilizando el argumento `end=''` de la misma función. De este y el anterior punto encontarán algunos detalles en el código de la solución oficial.

## Preguntas de los participantes

- Al ejecutar el programa directamente y darle los datos de entrada, la ventana se cierra «sin mostrar el resultado». En cambio si lo ejecuto desde el editor es posible ver el resultado.

En realidad al ejecutarlo directamente sí hace el proceso completo, el problema es que al estar en un entorno gráfico (en este caso en Windows), el programa se ejecuta en una terminal (**cmd**) que a su vez corre el intérprete de Python, pero al finalizar este último, la terminal también se cierra dado que su proceso *hijo* ya ha terminado, por eso da la sensación de que no ha mostrado el resultado. Esto mismo sucede al trabajar con otros lenguajes donde programamos aplicaciones de *linea de comandos* que son ejecutadas sobre entornos gráficos, en C por ejemplo tiende a utilizarse la instrucción **scanf** para esperar a que el usuario presione el teclado para cerrar la aplicación. En el caso de Python puede colocarse al final del programa la instrucción `input()`, así, sin argumentos, o con cualquier texto, entonces el programa no terminará hasta que el usuario presione la tecla **Enter** dado que justamente es una instrucción que espera la entrada del usuario.

La diferencia al ejecutarlo desde el editor, es que este gestiona de manera automática la terminal dejándo su salida visible, entonces aunque el proceso haya terminado podemos seguir visualizando el resultado. Es importante señalar que al usar `input()` al final del programa y ejecutarlo a través del editor, tendremos siempre que presionar **Enter** para que el programa termine, de lo contrario quedará *en pausa*.


## Solución

***
```python
'''
Este programa imprimirá dos veces el triángulo
debido a que se presentan dos implementaciones
distintas.
'''

# Definimos los valores por defecto
# Tamaño de la base
n = 5
# Caracter de dibujo
c = 'o'

# Preguntamos al usuario de qué tamaño quiere la base
# Las entradas desde el teclado siempre son de tipo cadena
n_str = input('¿De qué tamaño desea la base del triángulo? ')

# Intentamos convertir a entero
try:
	n = int(n_str)
except:
	# La f antes de la cadena nos permite interpolar expresiones
	# válidas del lenguaje, como variables, constantes, llamadas
	# a funciones, y más. Esto siempre que se encuentren encerradas
	# entre {}
	print(f'Entrada no válida. Usando el valor por default {n}')

# Preguntamos por el caracter de dibujo
c = input('¿Cuál es el caracter para dibujar? ')

# Dibujamos el triángulo
# Primero recorremos las filas
for i in range(0, n):
	# En Python3 existe el producto de cadenas por
	# un escalar (número entero en este caso), también,
	# se le llama el «operador de repetición»,
	# el resultado es la cadena repetida el número
	# de veces indicado por el escalar.
	# Aquí se requiere sumar 1 dado que el ciclo
	# comienza desde cero.
	print( c * (i+1) )

# Otro enfoque es utilizar ciclos anidados.
# Recorremos las filas
for i in range(0, n):
	# Recorremos las columnas
	for j in range(0,i+1):
		# La función print siempre agrega un salto
		# de linea al final, en este caso no lo
		# queremos así, entonces pasamos un parámetro
		# llamado *end* donde indicamos que al final
		# de su salida agregue una cadena vacía
		print(c, end='')

	# Luego, al terminar cada fila debemos saltar a la
	# siguiente. Para ello llamamos a print sin argumentos,
	# lo cual imprime una cadena vacía con un salto de
	# linea al final.
	print()


```
***
