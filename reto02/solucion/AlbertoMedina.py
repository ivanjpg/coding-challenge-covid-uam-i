#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
========================================================
          Triángulo con base de n cáracteres
========================================================
       
               13 / Abril / 2020   11:35
       
Autor: Alberto Medina   
"""

n = int(input("\n¿De qué tamaño se requiere la base del triángulo?\n"))
a = str(input("\n¿Con qué cáracter desea que dibujemos el triángulo?\n"))
print("\n")
for i in range(1,n+1):
    print(i*a)