# Reto 02
## Dibujando un triángulo con 5 caracteres

Este reto consiste en dibujar en pantalla un tríangulo rectángulo cuya base mida *5* caracteres. Como ejemplo consideremos que el caracter que utilizaremos para dibujar será **o**, entonces nuestro programa deberá tener como salida:

    o
    oo
    ooo
    oooo
    ooooo

**Extras**

- Se podría preguntar al usuario de qué tamaño se quiere la base del triángulo y con qué caracter desea que dibujemos el triángulo.
- También podrían leerse los parámetros anteriores desde la linea de comandos.

**Pistas**:

- En la mayoría de los lenguajes de programación, los ciclos (*loops*) son fundamentales. También existen los ciclos anidados.
- En Python3 existe el producto de cadenas por un escalar (número entero en este caso), el resultado es la cadena repetida el número de veces indicado por el escalar.