# Reto 03
## Implementando nuestras propias trigonométricas

Es común utilizar un montón de funciones trigonométricas desde la secundaria, aunque a estas alturas de la vida no calculamos de manera directa su valor numérico. Inclusive cuando programamos y recurrimos a ellas siempre están implementadas dentro de los lenguajes; si volvemos a la secundaria, ya están en las calculadoras. Sabemos que hay ciertos valores, que, usando el círculo unitario podemos calcular de manera exacta o dejar indicada con raíces y demás... ¿Pero cómo rayos calculan los valores en la computadora o calculadoras?

Es una pregunta que siempre me hice y desde la Universidad tuve la sensación de que lo hacían a través de series. Hace un par de años, quizás fue un domingo de flojera, me puse a buscar cómo lo hacían en C a sabiendas de que el código fuente estaría disponible, después de buscar un montón (la biblioteca es demasiado extensa), me encontré que efectivamente se calcula utilizando una serie de Taylor, evidentemente se incluyen correcciones debido a los errores de cálculo inherentes a la máquina y al truncamiento de la serie, es bastante complejo y sofisticado, pero en esencia sigue siendo una serie de Taylor, pueden consultar el código [acá](https://bit.ly/cccuami-glibc-sine).

No de manera tan detallada, pero ahora nosotros implementaremos las funciones trigonométricas básicas, en este caso seno, coseno y tangente. Hagamos una lista con lo que buscamos programar y sus características:

- Las funciones seno, coseno y tangente.
- Las dos primeras a través de series de Taylor.
- La tangente sin usar series, ya tenemos las otras dos, ¿verdad?
- Todos los ángulos de entrada, como la gente decente, serán en radianes.
- En el código de la biblioteca en C vemos que función seno considera hasta la potencia *9* de la serie de Taylor, es decir, los primeros cinco (5) términos no nulos de la serie, tomemos lo mismo para seno y coseno; implementemos las funciones tomando en cuenta hasta el quinto término no nulo de cada una de las series correspondientes.
- Tomemos por cierto que todos los ángulos que nos pediran serán $\theta \in [0,2 \pi]$.

## Extras

- Podrían implementar también cotangente, secante y cosecante, recordemos que son las recíprocas (**¡Aguas! no son inversas, son recíprocas**) de las que buscamos.
- Se podría preguntar al usuario de qué ángulo quiere calcular las funciones.
- También podrían leerse los ángulos desde la linea de comandos.
- Podrían implementarse las funciones para que reciban los ángulos en grados, el chiste sería hacerlo, además, sin repetir el código de conversión.
- ¿Qué pasa con $tan(\pi/2)$? Estaría genial considerar este caso.
- Podría preguntarse al usuario hasta qué término no nulo de la serie desea considerar y con base en ello hacer el cálculo.

## Pistas

- Seno y coseno, a través de series de Taylor, tienen la misma forma, lo único que cambia es en qué potencia inician, seno inicia en potencia 1, coseno inicia en potencia cero.
- Una referencia para funciones de Python: https://bit.ly/python-functions-tp
- Divide y vencerás, entre más funciones se definan y cada una de ellas realice trabajos más específicos y simples, se volverá más fácil el rastrear y corregir errores, además de que se evita la duplicidad de código.
- Las series de Taylor (Maclaurin) se pueden encontrar en la [Wikipedia](https://bit.ly/cccuami-maclaurin-trigonometric)
- [WolframAlpha](https://bit.ly/cccuami-wolframalpha) es un excelente recurso para que puedan comprobar la veracidad de sus resultados.