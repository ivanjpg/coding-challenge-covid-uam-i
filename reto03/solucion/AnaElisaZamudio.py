''' Calcular funciones trigonométricas utilizando una serie de taylor
seno, coseno, tangente, 

    *Con cualquier ángulo de entrada en radianes
    *Calcular: secante, cosecante y cotangente
    *Preguntar hasta qué término no nulo de la serie desea considerar
sin(x)= sum((-1)^n/(2n+1)! x^(2n+1)) para n=1 hasta n=variable?
cos(x)= sum((-1)^n/(2n)! x^(2n)) para n=1 hasta n=variable?
tan(x)=sin(x)/cos(x)
'''
import math
n1=8

def factorial(n): 
    resultado = 1
    i = 1
    while i <= n:
        resultado = resultado * i
        i = i + 1
    return resultado

x= math.pi

'''
Tú tenías: #sin(x)

Eso es un comentario, en realidad no definías una
función, entonces el return que tenías al final no
tenía sentido. Todo return debe estar dentro de una
función.

La b=0 que usas para guardar el resultado la tenías fuera,
debería estar dentro de la función.

En tu expresión de b += (-1)... te faltaba un paréntesis
que cerrara a tu llamada al factorial: 
factorial((2*i)+1)), te faltaba el último paréntesis antes
del operador *
'''
def sin(x):
    b = 0

    for i in range(1,n1):
        '''
        Tenías factorial((2*i)+1)) pero la i comienza
        en 1, entonces el primer número al que le calculas
        el factorial es (2*1)+1 = 3, pero para el seno
        debe ser 1!, entonces deberías hacer
        (2*1)-1 = 1 = 1!
        
        Lo mismo para la potencia de x
        Recuerda que el signo comienza siempre positivo,
        pero si tienes (-1)**i y tu primer i=1, entonces
        tu primer signo es negativo, deberías hacer
        (-1)**(i+1)
        
        Con todo y la corrección de los paréntesis del
        factorial, puedes valerte de la precedencia de
        operadores para evitar un poco de paréntesis.
        Primero se hacen las potencias. Las multiplicaciones
        y divisiones van después y tienen la misma
        precedencia.

        Considera que tu n1=8 no te da 8 términos no nulos
        porque estás brincando de dos en dos. Deberías poner
        16 para que te de 8 no nulos.

        También considera que range(1,n1) va de 1 hasta
        n1-1, no toca a n1; si quieres que llegue debes
        sumarle uno más.
        '''
        b += (-1)**(i+1) / factorial((2*i)-1) * (x**((2*i)-1))
    return(b)

'''
Tú tenías: print (math.sin(x))

- No dejes espacios entre el nombre de la función y el
paréntesis, que se vea así: print(math.sin(x))

- Si haces math.sin(x) estás llamando a la función seno
de la biblioteca de matemáticas de Python, entonces no
tiene chiste lo que tú hiciste.
'''
print(sin(x))