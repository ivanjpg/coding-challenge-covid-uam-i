
def fac(n):
    j=1
    fac=1
    while j<=n:
        fac *= j
        j=j+1
    return fac

def seno(x,t):
    sen=float()
    i=1
    j=1
    while i<=t:
      sen += (x**i)*((-1)**(j+1))/(fac(i))
      j=j+1
      i=i+2
    return sen

def coseno(x,t):
    cos=float()
    i=0
    j=1
    while i<=t:
      cos += (x**i)*((-1)**(j+1))/(fac(i))
      j=j+1
      i=i+2
    return cos

def tangente(x):
    if coseno(x,y)==0:
        tan="La tangente no esta definida"
        return tan
    else:
        tan=seno(x,y)/coseno(x,y)
        return tan

def cotangente(x):
    return 1/tangente(x)

def secante(x):
    return 1/coseno(x,y)

def cosecante(x):
    return 1/seno(x,y)

pi=3.14159265359

while True: 
    rad=input("Dame el angulo (en radianes como la gente decente, de 0 a 2pi): ")
    try: 
      x=float(rad)
      break
    except:
        print("Ese no es un numero.")
while True: 
    ter=input("¿Hasta que termino desea truncar las series?: ")
    try: 
      y=int(ter)
      break
    except:
        print("Ese no es un numero.")

if x>=pi:
    xn=x-pi
    print("El seno de "+ str(x) +"rad es "+ str(-seno(xn,y)))
    print("El coseno de "+ str(x) +"rad es "+ str(-coseno(xn,y)))
    print("La tangente de "+ str(x) +"rad es "+ str(tangente(xn)))
    print("La cotangente de "+ str(x) +"rad es "+ str(cotangente(xn)))
    print("La secante de "+ str(x) +"rad es "+ str(-secante(xn)))
    print("La cosecante de "+ str(x) +"rad es "+ str(-cosecante(xn)))
else:
    print("El seno de "+ str(x) +"rad es "+ str(seno(x,y)))
    print("El coseno de "+ str(x) +"rad es "+ str(coseno(x,y)))
    print("La tangente de "+ str(x) +"rad es "+ str(tangente(x)))
    print("La cotangente de "+ str(x) +"rad es "+ str(cotangente(x)))
    print("La secante de "+ str(x) +"rad es "+ str(secante(x)))
    print("La cosecante de "+ str(x) +"rad es "+ str(cosecante(x)))

##de aaron :))