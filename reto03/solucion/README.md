# Reto 03
## Implementando nuestras propias trigonométricas

Es común utilizar un montón de funciones trigonométricas desde la secundaria, aunque a estas alturas de la vida no calculamos de manera directa su valor numérico. Inclusive cuando programamos y recurrimos a ellas siempre están implementadas dentro de los lenguajes; si volvemos a la secundaria, ya están en las calculadoras. Sabemos que hay ciertos valores, que, usando el círculo unitario podemos calcular de manera exacta o dejar indicada con raíces y demás... ¿Pero cómo rayos calculan los valores en la computadora o calculadoras?

Es una pregunta que siempre me hice y desde la Universidad tuve la sensación de que lo hacían a través de series. Hace un par de años, quizás fue un domingo de flojera, me puse a buscar cómo lo hacían en C a sabiendas de que el código fuente estaría disponible, después de buscar un montón (la biblioteca es demasiado extensa), me encontré que efectivamente se calcula utilizando una serie de Taylor, evidentemente se incluyen correcciones debido a los errores de cálculo inherentes a la máquina y al truncamiento de la serie, es bastante complejo y sofisticado, pero en esencia sigue siendo una serie de Taylor, pueden consultar el código [acá](https://bit.ly/cccuami-glibc-sine).

No de manera tan detallada, pero ahora nosotros implementaremos las funciones trigonométricas básicas, en este caso seno, coseno y tangente. Hagamos una lista con lo que buscamos programar y sus características:

- Las funciones seno, coseno y tangente.
- Las dos primeras a través de series de Taylor.
- La tangente sin usar series, ya tenemos las otras dos, ¿verdad?
- Todos los ángulos de entrada, como la gente decente, serán en radianes.
- En el código de la biblioteca en C vemos que función seno considera hasta la potencia *9* de la serie de Taylor, es decir, los primeros cinco (5) términos no nulos de la serie, tomemos lo mismo para seno y coseno; implementemos las funciones tomando en cuenta hasta el quinto término no nulo de cada una de las series correspondientes.
- Tomemos por cierto que todos los ángulos que nos pediran serán $\theta \in [0,2 \pi]$.

## Extras

- Podrían implementar también cotangente, secante y cosecante, recordemos que son las recíprocas (**¡Aguas! no son inversas, son recíprocas**) de las que buscamos.
- Se podría preguntar al usuario de qué ángulo quiere calcular las funciones.
- También podrían leerse los ángulos desde la linea de comandos.
- Podrían implementarse las funciones para que reciban los ángulos en grados, el chiste sería hacerlo, además, sin repetir el código de conversión.
- ¿Qué pasa con $tan(\pi/2)$? Estaría genial considerar este caso.
- Podría preguntarse al usuario hasta qué término no nulo de la serie desea considerar y con base en ello hacer el cálculo.

## Pistas

- Seno y coseno, a través de series de Taylor, tienen la misma forma, lo único que cambia es en qué potencia inician, seno inicia en potencia 1, coseno inicia en potencia cero.
- Una referencia para funciones de Python: https://bit.ly/python-functions-tp
- Divide y vencerás, entre más funciones se definan y cada una de ellas realice trabajos más específicos y simples, se volverá más fácil el rastrear y corregir errores, además de que se evita la duplicidad de código.
- Las series de Taylor (Maclaurin) se pueden encontrar en la [Wikipedia](https://bit.ly/cccuami-maclaurin-trigonometric)
- [WolframAlpha](https://bit.ly/cccuami-wolframalpha) es un excelente recurso para que puedan comprobar la veracidad de sus resultados.

## Comentarios generales de las soluciones

- En este caso al subir un poco la complejidad del reto se nota mucho que todos pensamos diferente en la solución, resulta interesante ver que varios utilizaron bibliotecas externas para solucionar el problema, algunos para el factorial, otros sólo para tomar un valor ya dado de Pi. No lo considero mal, todo lo contrario, el utilizar herramientas específicas ya probadas nos permite enfocarnos mucho mejor en el problema que en realidad queremos resolver en lugar de reinventar la rueda. Busquen un programa que les guste, puede ser un App móvil, vayan a la sección de créditos y vean la cantidad inmensa de bibliotecas que utilizan, ¿por qué? Porque, por ejemplo, si están haciendo un App de correo electrónico no van a sentarse a reescribir la biblioteca de conexión de red, tampoco la que gestiona la base de datos interna, ... y así podríamos seguir enumerando. Si bien el escribir todo de cero es bastante formativo resulta poco práctico.

- Un añadido al comentario anterior es que me resulta curioso como varios tomaron el valor de Pi de Numpy, pero nadie tomó la constante directamente del módulo `math` incluído en Python.

- Varios elaboraron bastante sobre el manejo de errores revisando los casos especiales cuando algunas funciones van a infinito.

- Llama la atención que los que implementaron su propia función de factorial hayan considerado la versión no recursiva. Mi primer suposición sería que el multiplicar directamente los números es la manera operacional de visualizar al factorial. Aunque bien se pudo definir de manera recursiva, aunque computacionalmente acarrea problemas por el tamaño del stack.

## Solución

***
```python
# Función factorial
# en versión no recursiva, es decir,
# sabiendo que n! = n * (n-1) * (n-2) * ... * 1
def fact(n):
	# Sabemos que 0! = 1
	if n == 0:
		return 1

	# Definimos un neutro multiplicativo
	# donde guardar el resultado final
	# Usamos 1.0 para que f sea un flotante
	# ya que si el número es muy grande y luego
	# operamos con f entero y otros números de
	# tipo flotante, la conversión de f será
	# imposible.
	f = 1.0

	# Multiplicamos desde 2 hasta el
	# número pedido: 1 * 2 * 3 * 4 * ... * n
	for i in range(2, n+1):
		# f *= i es una manera corta de
		# escribir f = f * i
		f *= i

	return f

'''
Aquí trabajamos las funciones por separado
sólo para darnos cuenta que podemos unificarlas, por
ello se quedan como un graaaaan comentario

# La serie de taylor del seno
# considera sólo las potencias
# impares del polinomio.
# El parámetro n nos dice cuántos
# términos no nulos consideramos, por
# defecto consideramos 5
def seno(x, n=5):
	# Definimos un neutro aditivo para
	# guardar el resultado
	s = 0

	# Definimos una variable para alternar el signo
	# El primer término es positivo
	signo = +1
	# Calculamos los términos de la suma
	for i in range(1, 2*n, 2):
		s += signo * x**i / fact(i)
		# Alternamos el signo para la siguiente operación
		signo *= -1


	return s

# Para el coseno funciona de la
# misma manera que para el seno, sólo que
# ahora consideramos las potencias pares
# (incluyendo el cero)
def coseno(x, n=5):
	c = 0

	signo = +1
	for i in range(0, 2*n, 2):
		c += signo * x**i / fact(i)
		signo *= -1

	return c
'''

# Definimos una función general que
# puede calcular ambas funciones trigonométricas
# dado que sólo se diferencían en qué potencia
# comienzan; seno comienza con potencia 1, coseno
# comienza con potencia cero.
# El parámetro w nos dice qué función calcular.
def sincos(x, w, n=5):
	# Definimos un neutro aditivo para
	# guardar el resultado
	sc = 0.0

	# Definimos en que potencia iniciamos
	# establecemos un valor por default
	# a 1, que corresponde al seno
	p = 1
	# ¿Qué función calculamos?
	# Si nos indicaron coseno, cambiamos
	# la potencia de inicio a 0
	# Si es cualquier otra cosa asumimos
	# que se trata del seno, el valor
	# de la potencia ya se estableció para
	# ese caso
	if w == 'coseno':
		p = 0

	# Definimos una variable para alternar el signo
	# El primer término es positivo
	# El signo + no es necesario
	signo = +1
	# Calculamos los términos de la suma
	for i in range(p, 2*n, 2):
		sc += signo * x**i / fact(i)
		# Alternamos el signo para la siguiente operación
		signo *= -1


	return sc

# Definimos una función para cada cosa
# y que sea más fácil llamarlas
def seno(x, n=5):
	return sincos(x, 'seno', n)

def coseno(x, n=5):
	return sincos(x, 'coseno', n)

def tangente(x, n=5):
	return seno(x, n) / coseno(x, n)

def cotangente(x, n=5):
	return 1.0 / tangente(x, n)

def secante(x, n=5):
	return 1.0 / coseno(x, n)

def cosecante(x, n=5):
	return 1.0 / seno(x, n)


x_str = input('Ingrese un ángulo (en rads) entre 0 y 2pi: ')

# Valor por default de approx pi/6
x = 0.523598

# Términos no nulos de la serie
n = 10

# Generamos una lista con todas las funciones para
# llamarlas más fácilmente
fs = [seno, coseno, tangente, cotangente, secante, cosecante]

try:
	x = float(x_str)
except:
	print(f'Valor del ángulo no válido, usando por defecto {x}')

for f in fs:
	print(f'{f.__name__} de {x}rads = {f(x,n)}')
```
***