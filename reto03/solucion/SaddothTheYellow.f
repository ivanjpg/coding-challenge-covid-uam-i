      implicit double precision (u-z)
      implicit integer (t)
      
      write(*,*)"¿Qué funcion trigonométrica quieres calcular?"
      write(*,*)"Seno: escribe 1."
      write(*,*)"Coseno: escribe 2."
      write(*,*)"Tangente: escribe 3."
      write(*,*)"Cotangente: escribe 4."
      write(*,*)"Secante: escribe 5."
      write(*,*)"Cosecante: escribe 6."
      read(*,*)t
      write(*,*)" "
      write(*,*)"Ok ¿para qué ángulo? (en múltiplos de Pi)"
      read(*,*)z
      write(*,*)" "
      x=z*3.1415926535897932
      if (t==1) then
       write(*,*)"Habeis escogido seno."
       y=x-((x**3)/6)+((x**5)/120)-((x**7)/5040)+((x**9)/362880)

      elseif (t==2) then
       write(*,*)"Habeis escogido coseno."
       y=1-((x**2)/2)+((x**4)/24)-((x**6)/720)+((x**8)/40320)

      elseif (t==3) then
       write(*,*)"Habeis escogido tangente."
       u=x-((x**3)/6)+((x**5)/120)-((x**7)/5040)+((x**9)/362880)
       v=1-((x**2)/2)+((x**4)/24)-((x**6)/720)+((x**8)/40320)
       y=u/v

      elseif (t==4) then
       write(*,*)"Habeis escogido cotangente."
       u=x-((x**3)/6)+((x**5)/120)-((x**7)/5040)+((x**9)/362880)
       v=1-((x**2)/2)+((x**4)/24)-((x**6)/720)+((x**8)/40320)
       w=u/v
       y=1/w

      elseif (t==5) then
       write(*,*)"Habeis escogido secante."
       w=1-((x**2)/2)+((x**4)/24)-((x**6)/720)+((x**8)/40320)
       y=1/w

      elseif (t==6) then
       write(*,*)"Habeis escogido cosecante."
       w=x-((x**3)/6)+((x**5)/120)-((x**7)/5040)+((x**9)/362880)
       y=1/w

      else
       write(*,*)"Hostias tío ¿No sabeis leer? Te dije que escogieras 
     $del 1 al 6."
       write(*,*)"En respuesta, ahora vereis el número de la bestia."
       y=666
      endif

      write(*,*)" "
      write(*,*)"Aquí teneis:"
      write(*,*)y
      write(*,*)" "
      write(*,*)"Ahora, fuera de mi vista."
      end
