import numpy as np

x=float(input('Introduzca el ángulo que desea calcular, en radianes, de Seno, Coseno y Tangente'))


def Sen(x):
    x=x-((x**3)/6)+((x**5)/120)-((x**7)/5040)+((x**9)/362880)
    return x
VSen=Sen(x)

def Cos(x):
    x=1-((x**2)/2)+((x**4)/24)-((x**6)/720)+((x**8)/40320)-((x**10)/3628800)
    return x
VCos=Cos(x)

if x==3.14159265/2:
    print('infinito, si es que eso tiene sentido')
else:
    Tan=VSen/VCos


print()
print('Sin(%s)=%s'%(x,VSen))
print()
print('Cos(%s)=%s'%(x,VCos))
print()
print('Tan(%s)=%s'%(x,VSen/VCos))
print()
print('Csc(%s)=%s'%(x,1/VSen))
print()
print('Sec(%s)=%s'%(x,1/VCos))
print()
print('Cot(%s)=%s'%(x,Tan))
