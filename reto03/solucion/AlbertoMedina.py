#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 02:37:56 2020

@author: alberto
"""

"""
========================================================
  Implementando nuestras propias trigonométricas
========================================================
       
               14 / Abril / 2020    5:53
       
Autor: Alberto Medina   
"""
import numpy as np

def fct(k):
 fact =1.0   
 for i in range(k):
      fact = fact * float(i+1)
 return fact

def Sen(k,ang):
 if (ang == 0.0) or (ang == np.pi):
     sin = 0.0
 elif (ang == np.pi/2.0):
     sin = 1.0
 elif (ang == 3*np.pi/2.0):
     sin = -1.0
 else:
     sin = 0.0
     for i in range(k):
      a = (-1.0)**( i )
      b =  ang**( 2*i+1 )
      c =  fct( 2*i+1 )
      sin = sin + a * b / c
 return sin

def Cos(k,ang):
 if (ang == np.pi/2.0) or (ang == 3*np.pi/2.0):
     cos = 0.0
 elif (ang == 0.0):
     cos = 1.0
 elif (ang == np.pi ):
     cos = -1.0
 else: 
     cos = 0.0
     for i in range(k):
      d = (-1.0)**( i )
      e =  ang**( 2*i )
      f =  fct( 2*i )
      cos = cos + d * e / f
 return cos

def Tan(k,ang):
 tan = "Error"
 a = np.pi / 2.0
 b = 3.0 * np.pi / 2
 if ( ang != a ) and ( ang != b ):    
     tan = Sen(k,ang) / Cos(k,ang)
 return tan 

def Cot(k,ang):
 cot = "Error"
 a = 0.0
 b = np.pi
 if ( ang != a ) and ( ang != b ):    
     cot = Cos(k,ang) / Sen(k,ang)
 return cot

def Sec(k,ang):
 sec = "Error"
 a = np.pi / 2.0
 b = 3.0 * np.pi / 2
 if ( ang != a ) and ( ang != b ):    
     sec = 1.0 / Cos(k,ang)
 return sec

def Csc(k,ang):
 csc = "Error"
 a = 0.0
 b = np.pi
 if ( ang != a ) and ( ang != b ):    
     csc = 1.0 / Sen(k,ang)
 return csc


rg = str(input("Si el ángulo está en radianes introduzca 'y' de lo contrario introduzca 'n'.\n"))
if ( rg == 'y' ):
    tet  = float(input("Introduzca un número entre 0 y 2pi.\n"))
if ( rg == 'n' ):
    tet0 = float(input("Introduzca un número entre 0 y 360.\n"))
    tet = np.pi * tet0 / 180.0
n = int(input("Introduzca el número de términos para aproximar las funciones seno y coseno.\n"))
    
print("\nSen(x) =", Sen(n,tet))
print("\nCos(x) =", Cos(n,tet))    
print("\nTan(x) =", Tan(n,tet))
print("\nCot(x) =", Cot(n,tet))
print("\nSec(x) =", Sec(n,tet))
print("\nCsc(x) =", Csc(n,tet))
