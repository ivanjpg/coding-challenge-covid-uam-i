import numpy as np
import math

def coseno(angulo):
    
    delta = 1

    serie = 0

    i = 0

    while abs(delta) >= pow(10,-8):
        coseno = serie + pow(-1,i)*pow(angulo,2*i)/math.factorial(2*i)
        i += 1
        delta = coseno-serie 
        serie = coseno
        print(f"El valor del cos({angulo}) es {serie}, la serie se truncó en la iteración {i}")

coseno(np.pi)

# Ahora la función seno:

def seno(angulo):
    
    delta = 1

    serie = 0

    i = 0

    while abs(delta) >= pow(10,-8):
        coseno = serie + pow(-1,i)*pow(angulo,2*i +1)/math.factorial(2*i + 1)
        i += 1
        delta = coseno-serie 
        serie = coseno
        print(f"El valor del sin({angulo}) es {serie}, la serie se truncó en la iteración {i}")

seno(0)