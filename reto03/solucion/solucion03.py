# Función factorial
# en versión no recursiva, es decir,
# sabiendo que n! = n * (n-1) * (n-2) * ... * 1
def fact(n):
	# Sabemos que 0! = 1
	if n == 0:
		return 1

	# Definimos un neutro multiplicativo
	# donde guardar el resultado final
	# Usamos 1.0 para que f sea un flotante
	# ya que si el número es muy grande y luego
	# operamos con f entero y otros números de
	# tipo flotante, la conversión de f será
	# imposible.
	f = 1.0

	# Multiplicamos desde 2 hasta el
	# número pedido: 1 * 2 * 3 * 4 * ... * n
	for i in range(2, n+1):
		# f *= i es una manera corta de
		# escribir f = f * i
		f *= i

	return f

'''
Aquí trabajamos las funciones por separado
sólo para darnos cuenta que podemos unificarlas, por
ello se quedan como un graaaaan comentario

# La serie de taylor del seno
# considera sólo las potencias
# impares del polinomio.
# El parámetro n nos dice cuántos
# términos no nulos consideramos, por
# defecto consideramos 5
def seno(x, n=5):
	# Definimos un neutro aditivo para
	# guardar el resultado
	s = 0

	# Definimos una variable para alternar el signo
	# El primer término es positivo
	signo = +1
	# Calculamos los términos de la suma
	for i in range(1, 2*n, 2):
		s += signo * x**i / fact(i)
		# Alternamos el signo para la siguiente operación
		signo *= -1


	return s

# Para el coseno funciona de la
# misma manera que para el seno, sólo que
# ahora consideramos las potencias pares
# (incluyendo el cero)
def coseno(x, n=5):
	c = 0

	signo = +1
	for i in range(0, 2*n, 2):
		c += signo * x**i / fact(i)
		signo *= -1

	return c
'''

# Definimos una función general que
# puede calcular ambas funciones trigonométricas
# dado que sólo se diferencían en qué potencia
# comienzan; seno comienza con potencia 1, coseno
# comienza con potencia cero.
# El parámetro w nos dice qué función calcular.
def sincos(x, w, n=5):
	# Definimos un neutro aditivo para
	# guardar el resultado
	sc = 0.0

	# Definimos en que potencia iniciamos
	# establecemos un valor por default
	# a 1, que corresponde al seno
	p = 1
	# ¿Qué función calculamos?
	# Si nos indicaron coseno, cambiamos
	# la potencia de inicio a 0
	# Si es cualquier otra cosa asumimos
	# que se trata del seno, el valor
	# de la potencia ya se estableció para
	# ese caso
	if w == 'coseno':
		p = 0

	# Definimos una variable para alternar el signo
	# El primer término es positivo
	# El signo + no es necesario
	signo = +1
	# Calculamos los términos de la suma
	for i in range(p, 2*n, 2):
		sc += signo * x**i / fact(i)
		# Alternamos el signo para la siguiente operación
		signo *= -1


	return sc

# Definimos una función para cada cosa
# y que sea más fácil llamarlas
def seno(x, n=5):
	return sincos(x, 'seno', n)

def coseno(x, n=5):
	return sincos(x, 'coseno', n)

def tangente(x, n=5):
	return seno(x, n) / coseno(x, n)

def cotangente(x, n=5):
	return 1.0 / tangente(x, n)

def secante(x, n=5):
	return 1.0 / coseno(x, n)

def cosecante(x, n=5):
	return 1.0 / seno(x, n)


x_str = input('Ingrese un ángulo (en rads) entre 0 y 2π: ')

# Valor por default de approx pi/6
x = 0.523598

# Términos no nulos de la serie
n = 10

# Generamos una lista con todas las funciones para
# llamarlas más fácilmente
fs = [seno, coseno, tangente, cotangente, secante, cosecante]

try:
	x = float(x_str)
except:
	print(f'Valor del ángulo no válido, usando por defecto {x}')

for f in fs:
	print(f'{f.__name__} de {x}rads = {f(x,n)}')